﻿using Northwind.Data;
using Northwind.WebApi.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace Northwind.WebApi.Controllers
{
    public class CustomerSearchController : ApiController, ICustomerSearchService
    {
        private readonly ICustomerSearchRepository _customerSearchRepository;

        public CustomerSearchController(ICustomerSearchRepository customerSearchRepository)
        {
            _customerSearchRepository = customerSearchRepository;
        }
        [HttpPost]
        public async Task<IEnumerable<string>> GetCustomersNamesAsync(SearchOrderCriteriaDTO criteria)
        {
            if (criteria.LessOrEqualOrders.HasValue)
            {
                var result = await _customerSearchRepository.GetCustomersWithLessOrEqualNumberOfOrdersAsync(criteria.LessOrEqualOrders.Value);
                return result;
            }
            return new List<string>();
        }

    }
}