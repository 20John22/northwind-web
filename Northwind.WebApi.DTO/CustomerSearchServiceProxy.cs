﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace Northwind.WebApi.DTO
{
    public class CustomerSearchServiceProxy : ICustomerSearchService
    {
        private readonly string _baseAddress;
        public CustomerSearchServiceProxy(string baseAddress)
        {
            _baseAddress = baseAddress;
        }
        public async Task<IEnumerable<string>> GetCustomersNamesAsync(SearchOrderCriteriaDTO criteria)
        {
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders
                  .Accept
                  .Add(new MediaTypeWithQualityHeaderValue("application/json"));

            var response = await client.PostAsJsonAsync(_baseAddress + "api/CustomerSearch", criteria);

            return await response.Content.ReadAsAsync<List<string>>();

        }
    }
}
