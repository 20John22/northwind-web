﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Northwind.WebApi.DTO
{
    public interface ICustomerSearchService
    {
        Task<IEnumerable<string>> GetCustomersNamesAsync(SearchOrderCriteriaDTO criteria);
    }
}