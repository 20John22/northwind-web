﻿namespace Northwind.WebApi.DTO
{
    public class SearchOrderCriteriaDTO
    {
        public int? LessOrEqualOrders { get; set; }
    }
}