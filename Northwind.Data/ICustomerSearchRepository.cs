﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Northwind.Data
{
    public interface ICustomerSearchRepository
    {
        Task<IEnumerable<string>> GetCustomersWithLessOrEqualNumberOfOrdersAsync(int lessOrEqualNumberOfOrders);
    }
}