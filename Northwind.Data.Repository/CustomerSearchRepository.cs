﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Northwind.Data.Repository
{
    public class CustomerSearchRepository : ICustomerSearchRepository
    {
        private readonly string _connectionString;
        public CustomerSearchRepository(string connectionString)
        {
            _connectionString = connectionString;
        }

        //TODO: do we need paging? shall we parepare for more days in requirement?
        public async Task<IEnumerable<string>> GetCustomersWithLessOrEqualNumberOfOrdersAsync(int lessOrEqualNumberOfOrders)
        {
            if (lessOrEqualNumberOfOrders < 0)
                throw new ArgumentException("lessOrEqualNumberOfOrders cannot be less then 0");

            var p = new DynamicParameters();
            p.Add("@lessOrEqualNumberOfOrders", lessOrEqualNumberOfOrders);

            using (IDbConnection cnn = new SqlConnection(_connectionString))
            {
                //for simplicity script is in repository layer, in n-tier architecture this logic would be in sproc
                string sprocName = @"SELECT c.ContactName FROM Customers c
                                 WHERE(select count(*) as numberOfOrders from Orders o where c.CustomerID = o.CustomerID) <= @lessOrEqualNumberOfOrders";

                return await cnn.QueryAsync<string>(sprocName, p);
            }
        }
    }
}
