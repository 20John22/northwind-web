﻿using Autofac;
using Microsoft.Owin.Hosting;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Northwind.Data;
using Northwind.WebApi;
using Northwind.WebApi.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Northwind.Data.Repository.Test
{
    [TestClass]
    public class CustomerSearchServiceTest : Startup
    {
        private static Mock<ICustomerSearchRepository> _customerSearchRepositoryMock = new Mock<ICustomerSearchRepository>();
        protected override void Register(ContainerBuilder builder)
        {
            builder.Register(c => _customerSearchRepositoryMock.Object)
                .As<ICustomerSearchRepository>();
        }

        [TestMethod]
        [TestCategory("Service")]
        public async Task Returns_Customers_Names_For_Criteria()
        {
            string baseAddress = "http://localhost:9004/";

            //Arrange
            SearchOrderCriteriaDTO criteria = new SearchOrderCriteriaDTO { LessOrEqualOrders = 1 };

            List<string> customersNames = new List<string> { "test1", "test2" };
            _customerSearchRepositoryMock.Setup(s => s.GetCustomersWithLessOrEqualNumberOfOrdersAsync(criteria.LessOrEqualOrders.Value)).ReturnsAsync(customersNames);

            using (WebApp.Start<CustomerSearchServiceTest>(url: baseAddress))
            {
                CustomerSearchServiceProxy proxy = new CustomerSearchServiceProxy(baseAddress);

                //Act
                var result = await proxy.GetCustomersNamesAsync(criteria);

                //Assert
                Assert.IsNotNull(result);
                Assert.AreEqual(customersNames.Count, result.Count());
                _customerSearchRepositoryMock.Verify(r => r.GetCustomersWithLessOrEqualNumberOfOrdersAsync(criteria.LessOrEqualOrders.Value), Times.Once);
            }
        }

        [TestMethod]
        [TestCategory("Service")]
        public async Task Returns_Empty_For_Not_Matched_Criteria()
        {
            string baseAddress = "http://localhost:9004/";

            //Arrange
            SearchOrderCriteriaDTO criteria = new SearchOrderCriteriaDTO { LessOrEqualOrders = 0 };

            List<string> customersNames = new List<string> { };
            _customerSearchRepositoryMock.Setup(s => s.GetCustomersWithLessOrEqualNumberOfOrdersAsync(criteria.LessOrEqualOrders.Value)).ReturnsAsync(customersNames);

            using (WebApp.Start<CustomerSearchServiceTest>(url: baseAddress))
            {
                CustomerSearchServiceProxy proxy = new CustomerSearchServiceProxy(baseAddress);

                //Act
                var result = await proxy.GetCustomersNamesAsync(criteria);

                //Assert
                Assert.IsNotNull(result);
                Assert.AreEqual(customersNames.Count, result.Count());
                //_customerSearchRepositoryMock.Verify(r => r.GetCustomersWithLessOrEqualNumberOfOrdersAsync(criteria.LessOrEqualOrders.Value), Times.Once);
            }
        }

        [TestMethod]
        [TestCategory("Service")]
        public async Task Returns_Empty_For_Invalid_Criteria()
        {
            string baseAddress = "http://localhost:9004/";

            //Arrange
            SearchOrderCriteriaDTO criteria = new SearchOrderCriteriaDTO { LessOrEqualOrders = null };

            List<string> customersNames = new List<string> { "test1", "test2" };
            _customerSearchRepositoryMock.Setup(s => s.GetCustomersWithLessOrEqualNumberOfOrdersAsync(It.IsAny<int>())).ReturnsAsync(customersNames);

            using (WebApp.Start<CustomerSearchServiceTest>(url: baseAddress))
            {
                CustomerSearchServiceProxy proxy = new CustomerSearchServiceProxy(baseAddress);

                //Act
                var result = await proxy.GetCustomersNamesAsync(criteria);

                //Assert
                Assert.IsNotNull(result);
                Assert.AreEqual(0, result.Count());
               // _customerSearchRepositoryMock.Verify(r => r.GetCustomersWithLessOrEqualNumberOfOrdersAsync(criteria.LessOrEqualOrders.Value), Times.Never);
            }
        }
    }
}
