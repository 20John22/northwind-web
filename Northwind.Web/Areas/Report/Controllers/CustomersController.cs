﻿using Newtonsoft.Json;
using Northwind.WebApi.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Northwind.Web.Areas.Report.Controllers
{
    //[ClaimsAuthorize(ClaimTypes.Role,"Admin", "Manager")]
    public class CustomersController : Controller
    {
        private readonly ICustomerSearchService _customerSearchService;

        public CustomersController(ICustomerSearchService customerSearchService)
        {
            _customerSearchService = customerSearchService;
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public async Task<ViewResult> OneOrZeroOrders()
        {
            IEnumerable<string> customersNames = await _customerSearchService.GetCustomersNamesAsync(new SearchOrderCriteriaDTO { LessOrEqualOrders = 1 });
            return View(customersNames);
        }

        [HttpGet]
        public ViewResult OneOrZeroOrdersWithAngular()
        {
            ViewBag.RootUrl = GetRootUrl();
            return View();
        }

        [HttpGet]
        public async Task<ViewResult> OneOrZeroOrdersWithReact()
        {
            IEnumerable<string> customersNames = await _customerSearchService.GetCustomersNamesAsync(new SearchOrderCriteriaDTO { LessOrEqualOrders = 1 });

            ViewBag.Customers = JsonConvert.SerializeObject(customersNames.Select((s, i) => new { customer = s }));
            ViewBag.RootUrl = GetRootUrl();

            return View();
        }

        private string GetRootUrl()
        {
            var appPath = string.Empty;
            var context = HttpContext;
            if (context != null)
            {
                appPath = string.Format("{0}://{1}{2}{3}",
                                        context.Request.Url.Scheme,
                                        context.Request.Url.Host,
                                        context.Request.Url.Port == 80
                                            ? string.Empty
                                            : ":" + context.Request.Url.Port,
                                        context.Request.ApplicationPath);
            }

            if (!appPath.EndsWith("/"))
                appPath += "/";

            return appPath;
        }

        [HttpGet]
        public async Task<JsonResult> GetCustomersNames(int lrange, int rrange)
        {
            IEnumerable<string> customersNames = await _customerSearchService.GetCustomersNamesAsync(new SearchOrderCriteriaDTO { LessOrEqualOrders = 1 });
            return Json(new { Data = customersNames.Select(s => new { name = s}) }, JsonRequestBehavior.AllowGet);
        }
    }
}