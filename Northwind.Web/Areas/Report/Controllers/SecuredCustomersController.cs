﻿using Northwind.WebApi.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Northwind.Web.Areas.Report.Controllers
{
    [Authorize]
    public class SecuredCustomersController : Controller
    {
        private readonly ICustomerSearchService _customerSearchService;

        public SecuredCustomersController(ICustomerSearchService customerSearchService)
        {
            _customerSearchService = customerSearchService;
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public async Task<ViewResult> OneOrZeroOrders()
        {
            ViewBag.Message = "Only for authenticated users!";

            IEnumerable<string> customersNames = await _customerSearchService.GetCustomersNamesAsync(new SearchOrderCriteriaDTO { LessOrEqualOrders = 1 });
            return View("OneOrZeroOrders", customersNames);
        }
    }
}