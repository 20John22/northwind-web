﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Northwind.Data.Repository;
using System.Linq;
using Northwind.Data;
using System.Configuration;
using System.Threading.Tasks;

namespace Northwind.Data.Repository.Test
{
    [TestClass]
    public class CustomerSearchRepositoryTest
    {
        private readonly string _connectionString = ConfigurationManager.ConnectionStrings["Northwind"].ConnectionString;
        //TODO prepare database

        [TestMethod]
        [TestCategory("Repository")]
        public async Task Returns_Customers_With_1_Or_0_Orders()
        {
            //Arrange
            ICustomerSearchRepository repository = new CustomerSearchRepository(_connectionString);

            //Act
            var result = await repository.GetCustomersWithLessOrEqualNumberOfOrdersAsync(1);

            //Assert
            Assert.AreEqual(3, result.Count());
            //collection testing with nunit seems easier, unfortunelty nunit have requirements on extension for VS
            Assert.IsNotNull(result.First());
        }

        [TestMethod]
        [TestCategory("Repository")]
        public async Task Returns_Customers_With_0_Orders()
        {
            //Arrange
            CustomerSearchRepository repository = new CustomerSearchRepository(_connectionString);

            //Act
            var result = await repository.GetCustomersWithLessOrEqualNumberOfOrdersAsync(0);

            //Assert
            Assert.AreEqual(2, result.Count());
        }

        [ExpectedException(typeof(ArgumentException))]
        [TestMethod]
        [TestCategory("Repository")]
        public async Task Throws_Exception_For_Less_Then_0()
        {
            //Arrange
            CustomerSearchRepository repository = new CustomerSearchRepository(_connectionString);

            //Act & Assert
            var result = await repository.GetCustomersWithLessOrEqualNumberOfOrdersAsync(-1);
        }
    }
}
