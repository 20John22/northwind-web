﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Northwind.Data;
using Moq;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Linq;
using Northwind.Web.Areas.Report.Controllers;
using Northwind.WebApi.DTO;
using System.Threading.Tasks;

namespace Northwind.Web.Test
{
    [TestClass]
    public class CustomersControllerTest
    {
        [TestMethod]
        [TestCategory("Controller")]
        public async Task Returns_CustomersNames()
        {
            //Arrange
            Mock<ICustomerSearchService> serviceMock = new Mock<ICustomerSearchService>();

            CustomersController controller = new CustomersController(serviceMock.Object);
            serviceMock.Setup(s => s.GetCustomersNamesAsync(It.IsAny<SearchOrderCriteriaDTO>())).ReturnsAsync(new List<string> { "test1", "test2" });

            //Act
            ViewResult result = await controller.OneOrZeroOrders();

            //Assert
            var customerNames = (IEnumerable<string>)result.ViewData.Model;
            Assert.AreEqual(2, customerNames.Count());
        }
    }
}
